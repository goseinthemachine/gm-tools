class RoomReflex < StimulusReflex::Reflex
  def add_pc

  end

  def add_npc

  end

  def increment_initiative

  end

  def start_encounter

  end

  def reload
    byebug
  end

  private
  def increment_round
  end

  def roll_initiative

  end

  def get_room

  end

  def save_room
      @room = Rails.cache.read(params[:room]) || Room.new(room: element.dataset[:room])
  end
end