class CounterReflex < StimulusReflex::Reflex
  def increment

    @count = Rails.cache.read(element.dataset[:room]) || 0
    @count = @count.to_i + element.dataset[:step].to_i
    Rails.cache.write element.dataset[:room], @count
  end
end