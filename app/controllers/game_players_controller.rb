class GamePlayersController < ApplicationController

  def new
    get_room(params[:home_game_room])
    @game_player = GamePlayer.new
    @game_player.session = session.id
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    get_room(params[:home_game_room])

    if is_game_master?
      npc = GamePlayer.new(session: post_params[:session], player_name: post_params[:player_name], character_name: post_params[:character_name], init_bonus: post_params[:init_bonus])
      @room.players.push(npc)
      Rails.cache.write params[:home_game_room], @room
      RoomChannel.broadcast_to("room_#{params[:home_game_room]}", "Update Game Board")

    elsif !is_player?

      player = GamePlayer.new(session: post_params[:session], player_name: post_params[:player_name], character_name: post_params[:character_name], init_bonus: post_params[:init_bonus])
      player.session = session.id
      @room.players.push(player)

      Rails.cache.write params[:home_game_room], @room
      RoomChannel.broadcast_to("room_#{params[:home_game_room]}", "Update Game Board")
    end
  end

  def edit
    get_room(params[:home_game_room])
    idx = @room.players.find_index { |p| p.id == params[:id]}
    @game_player = @room.players[idx]
    respond_to do |format|
      format.js
    end
  end

  def update
    get_room(params[:home_game_room])
    idx = @room.players.find_index { |p| p.id == params[:id]}
    player_to_update = @room.players[idx]
    player_to_update.player_name = update_params[:player_name]
    player_to_update.character_name = update_params[:character_name]
    player_to_update.init_bonus = update_params[:init_bonus]
    @room.players[idx] = player_to_update
    save_changes(@room)
  end



  def is_game_master?
    @room.game_master.session.to_s == session.id.to_s
  end

  def is_player?
    @room.players.any? { |player| player.session.to_s == session.id.to_s}
  end

  def session_exists?
    return is_game_master? ||
        is_player?
  end

  def save_changes(room)

    Rails.cache.write params[:home_game_room], room
    RoomChannel.broadcast_to("room_#{params[:home_game_room]}",
                             title: 'stuff happened!')
  end

  def post_params
    params.require(:game_player).permit(:session, :player_name, :character_name, :init_bonus)
  end

  def update_params
    params.required(:game_player).permit(:player_name, :character_name, :init_bonus)
  end
end
