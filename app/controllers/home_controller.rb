class HomeController < ApplicationController
  include CableReady::Broadcaster

  def index
  end

  def show
    get_room(params[:game_room])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def is_game_master?
    @room.game_master.session.to_s == session.id.to_s
  end

  def is_player?
    @room.players.any? { |player| player.session.to_s == session.id.to_s}
  end

  def session_exists?
    return is_game_master? ||
        is_player?
  end
end
