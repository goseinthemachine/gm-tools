class ApplicationController < ActionController::Base
  def get_room(game_room)
    @room = Rails.cache.read(game_room) || Room.new(room: params[:home_game_room], session: session.id, name: "")
    Rails.cache.write game_room, @room
  end
end
