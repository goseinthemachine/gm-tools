class RoundsController < ApplicationController

  def create
    get_room(params[:home_game_room])
    @room.round = 1
    @room.dynamic_initiative = update_params[:dynamic_initiative]
    roll_initiative

  end

  def update
    get_room(params[:home_game_room])

    @room.dynamic_initiative = update_params[:dynamic_initiative]
    step_initiative
  end

  def save_changes(room)
    Rails.cache.write params[:home_game_room], room
    RoomChannel.broadcast_to("room_#{params[:home_game_room]}", title: 'Update Gameboard')
  end

  def update_params
    params.require(:room).permit(:dynamic_initiative)
  end

  def step_initiative

    if @room.players.last.id == @room.active_player
      idx = 0
      @room.round += 1

      if @room.dynamic_initiative.to_i > 0
        roll_initiative
      end
    else
      idx = @room.players.index { |player| player.id.to_s == @room.active_player } + 1
    end

    @room.active_player = @room.players[idx].id

    save_changes(@room)
  end

  def roll_initiative
    @room.players.each do |player|
      player.current_initiative = rand(1..20) + player.init_bonus.to_i
    end
    # sorted_players = @room.players.sort { |player| player.current_initiative.to_i }
    sorted_players = @room.players.sort do |a, b|
      if a.current_initiative == b.current_initiative && a.init_bonus.to_i > b.init_bonus.to_i
        -1
      elsif a.current_initiative > b.current_initiative
        -1
      elsif a.current_initiative < b.current_initiative
        1
      else
        0
      end
    end
    @room.players = sorted_players
    @room.active_player = sorted_players.first.nil? ? nil : sorted_players.first.id
    save_changes(@room)
  end
end
