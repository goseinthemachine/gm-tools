import Rails from '@rails/ujs'
import {debounce} from 'lodash-es'
import ApplicationController from './application_controller'
import consumer from "../channels/consumer";

const reload = controller => {
    controller.stimulate("RoomReflex#reload")
}

// const debouncedReload = debounce(reload, 100)

export default class extends ApplicationController{
    connect (){
        // super.connect();
        debugger;
        let gameRoom = this.data.get("game-room")
        console.log(gameRoom)
        consumer.subscriptions.create({ channel: "RoomChannel", game_room: gameRoom },
            {
                received(data){
                    console.log(data)

                    Rails.ajax({
                        type: "get",
                        url: window.location.href,
                        dataType: 'script',
                        success: function(response){
                            console.log(response);
                        },

                    });
                }
            });

    }

    reload(){
        console.log("Reloading...")
        debouncedReload()
    }
}