class RoomChannel < ApplicationCable::Channel
  def subscribed
    stream_for "room_#{params[:game_room]}"
  end
end