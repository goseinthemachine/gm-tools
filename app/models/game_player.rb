class GamePlayer
  include ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  attr_accessor :session, :player_name, :character_name, :init_bonus, :current_initiative, :id

  def initialize(session: session, player_name: player_name, character_name: character_name, init_bonus: init_bonus)
    @id = SecureRandom.uuid
    @session = session
    @player_name = player_name
    @character_name = character_name
    @init_bonus = init_bonus
    @current_initiative = 0
  end

  def persisted?
    false
  end
end
