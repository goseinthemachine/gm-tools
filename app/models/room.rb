class Room
  include ActiveModel::Model
  attr_accessor :game_master,
                :number,
                :players,
                :npcs,
                :round,
                :active_player,
                :dynamic_initiative

  def initialize(room: room, session: session, name: name)
    @number = room
    @game_master = GameMaster.new(session: session, name: name)
    @players = []
    @round = 0
    @active_player = ""
    @dynamic_initiative = false

  end
end