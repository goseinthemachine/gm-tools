class GameMaster
  attr_accessor :session, :name

  def initialize(session: session, name: name)
    @session = session
    @name = name
  end
end