module ApplicationHelper
  def is_game_master?(room)
    room.game_master.session.to_s == session.id.to_s
  end

  def is_player?(room)
    room.players.any? { |player| player.session.to_s == session.id.to_s}
  end

  def user_in_game?(room)
    is_player?(room) || is_game_master?(room)
  end
end
