Rails.application.routes.draw do


  resources :home, param: :game_room do
    resources :game_players
    resources :rounds
  end


  root 'home#index'
end
